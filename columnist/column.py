import functools
from numbers import Number
from typing import AnyStr
from typing import Callable
from typing import Optional
from typing import Union


class ColumnDescriptor:
    def __init__(
        self,
        func: Callable,
        label: Optional[AnyStr]=None
    ):
        self._label = label
        self.func = func

    def __call__(self, record: object) -> Union[Number, AnyStr]:
        return self.func(self, record)

    @property
    def label(self) -> str:
        if self._label is not None:
            return self._label
        return self.func.__name__


def column(*args, **kwargs) -> ColumnDescriptor:
    if len(args) == 1 and callable(args[0]):
        return ColumnDescriptor(args[0])

    p = functools.partial(
        ColumnDescriptor,
        **kwargs
    )
    return p
