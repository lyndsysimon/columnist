import abc

import columnist


class Formatter(metaclass=abc.ABCMeta):
    def __init__(self, report: columnist.Report):
        self.report = report

    @abc.abstractmethod
    def to_file(self, destination, **options) -> None:
        """Write the formatted report to a file"""
        pass

    @abc.abstractmethod
    def to_string(self, **options) -> str:
        """Return the formatted report as a string"""
        pass
