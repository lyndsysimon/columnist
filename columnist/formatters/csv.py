import csv
import io

from .base import Formatter


class CSVFormatter(Formatter):
    def to_file(self, destination: io.TextIOWrapper, **options):
        column_labels = [
            column.label
            for column
            in self.report.columns
        ]
        writer = csv.DictWriter(
            f=destination,
            fieldnames=column_labels,
            **options,
        )
        writer.writeheader()
        for row in self.report.rows():
            writer.writerow(row)

    def to_string(self, **options):
        output = io.StringIO()
        self.to_file(output, **options)
        return output.getvalue()
