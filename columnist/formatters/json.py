import json

from .base import Formatter


class JSONFormatter(Formatter):
    def to_file(self, destination, **options):
        destination.write(self.to_string(**options))

    def to_string(self, **options):
        return json.dumps(list(self.report.rows()))
