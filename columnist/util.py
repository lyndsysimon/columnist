class ClassPropertyDescriptor:
    def __init__(self, fget, fset=None):
        self.fget = fget
        self.fset = fset

    def __get__(self, obj, cls=None):
        return self.fget.__get__(
            obj,
            type(obj) if cls is None else cls,
        )()

    def __set__(self, obj, value):
        if not self.fset:
            raise AttributeError('Cannot set attribute')
        return self.fset.__get__(obj, type(obj))(value)

    def setter(self, func):
        if not isinstance(func, (classmethod, staticmethod)):
            func = classmethod(func)
        self.fset = func
        return self


def classproperty(func):
    if not isinstance(func, (classmethod, staticmethod)):
        func = classmethod(func)
    return ClassPropertyDescriptor(func)
