import abc
import enum
import re

from .column import ColumnDescriptor
from .util import classproperty


class RowPer(enum.Enum):
    ROW = 'row'
    DAY = 'day'
    WEEK = 'week'
    MONTH = 'month'


class Report(metaclass=abc.ABCMeta):
    NAME = None
    ROW_PER = RowPer.ROW
    # TODO: Ensure all columns - and only columns - are included
    COLUMN_ORDER = None

    @classproperty
    def _name(cls) -> str:
        """The name of the class; used to select a specific subclass"""
        if cls.NAME is not None:
            return cls.NAME

        name = cls.__name__

        # strip 'Report' suffix
        if name.endswith('Report'):
            name = name[:-6]

        return re.sub('(.)([A-Z].+?)', r'\1_\2', name).lower()

    @classproperty
    def columns(cls):
        columns = set([
            x for
            x in cls.__dict__.values()
            if isinstance(x, ColumnDescriptor)
        ])
        if cls.COLUMN_ORDER is None:
            return columns
        else:
            return [
                [
                    column
                    for column
                    in columns
                    if column.label == label
                ][0]
                for label
                in cls.COLUMN_ORDER
            ]

    @abc.abstractmethod
    def query(self):
        pass

    def to_dict(self, record):
        return {
            col.label: col(record)
            for col
            in self.columns
        }

    def rows(self):
        return map(self.to_dict, self.query())
