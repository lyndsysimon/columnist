import os.path
import sys

from .db import Base
from .db import engine
from .db import session

import pytest


# Ensure the source root is in the path
source_root = os.path.abspath('{}/..'.format(os.path.dirname(__file__)))

if source_root not in sys.path:
    sys.path.insert(0, source_root)


@pytest.fixture
def db():
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
    session.commit()
    return session
