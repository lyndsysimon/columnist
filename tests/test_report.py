import pytest

from columnist import Report
from columnist import column
from columnist.column import ColumnDescriptor
from tests.db import Model


class MockReport(Report):
    @column
    def alpha_is_foo(self, record):
        return record.alpha == 'foo'

    @column(label='Date Created')
    def created(self, record):
        return record.created.strftime('%Y-%m-%d')

    query = Model.query.all


class TestReport:
    @pytest.fixture
    def records(self, db):
        records = (
            Model(alpha='foo'),
            Model(alpha='bar'),
            Model(alpha='foo'),
        )
        db.add_all(records)
        db.commit()
        return records

    @pytest.fixture
    def expected(self, records):
        return [
            {
                'alpha_is_foo': record.alpha == 'foo',
                'Date Created': record.created.strftime('%Y-%m-%d'),
            } for record in records
        ]

    def test_name_from_property(self):
        assert MockReport._name == 'mock'

    @pytest.mark.parametrize(
        'class_name,expected',
        (
            ('FooReport', 'foo'),
            ('FooBarReport', 'foo_bar'),
            ('FooBarBazReport', 'foo_bar_baz'),
        ),
    )
    def test_name_from_class(self, class_name, expected):
        """Derives the correct name from the class"""
        cls = type(class_name, (Report, ), {})
        assert cls._name == expected

    def test_columns(self):
        """Methods with the @column decorator are tracked"""
        assert len(MockReport.columns) == 2
        for col in MockReport.columns:
            assert isinstance(col, ColumnDescriptor)

    def test_query(self, records):
        """Query returns all expected rows"""
        assert len(MockReport().query()) == 3

    def test_to_dict(self, records, expected):
        """Report.to_dict() properly transforms each record"""
        for record, expected in zip(records, expected):
            assert MockReport().to_dict(record) == expected

    def test_rows(self, expected):
        """Report.rows() returns all transformed rows"""
        assert list(MockReport().rows()) == expected
