import textwrap

import pytest

from columnist.column import column
from columnist.column import ColumnDescriptor


class TestColumn:
    @pytest.mark.parametrize(
        'name',
        ('foo', 'bar', 'baz', 'qiz'),
    )
    def test_label_from_name(self, name):
        function_definition = textwrap.dedent(
            """
            @column
            def {}():
                pass
            """.format(name),
        )
        exec(function_definition, globals(), locals())
        function = locals()[name]

        assert isinstance(function, ColumnDescriptor)
        assert function.func.__name__ == name
        assert function.label == name

    @pytest.mark.parametrize(
        'name',
        ('foo', 'bar', 'baz', 'qiz'),
    )
    def test_label_explicit(self, name):
        @column(label=name)
        def function():
            pass

        assert isinstance(function, ColumnDescriptor)
        assert function.func.__name__ == 'function'
        assert function.label == name
