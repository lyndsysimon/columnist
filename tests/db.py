import datetime

import sqlalchemy as sqla
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


engine = sqla.create_engine('sqlite:///:memory:')
session = sessionmaker(bind=engine)()
Base = declarative_base()


class Model(Base):
    __tablename__ = 'model'

    id = sqla.Column(sqla.Integer, primary_key=True)
    created = sqla.Column(
        sqla.DateTime,
        nullable=False,
        default=datetime.datetime.now,
    )
    updated = sqla.Column(
        sqla.DateTime,
        nullable=False,
        default=datetime.datetime.now,
    )
    alpha = sqla.Column(sqla.String)
    bravo = sqla.Column(sqla.String)
    charlie = sqla.Column(sqla.String)


Model.query = session.query(Model)
