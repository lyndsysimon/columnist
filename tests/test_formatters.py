import datetime
import json

import pytest

from columnist import column
from columnist import Report
from columnist.formatters import CSVFormatter
from columnist.formatters import JSONFormatter
from tests.db import Model


class MockReport(Report):
    @column
    def alpha_is_foo(self, record):
        return record.alpha == 'foo'

    @column(label='Date Created')
    def created(self, record):
        return record.created.strftime('%Y-%m-%d')

    query = Model.query.all

    COLUMN_ORDER = (
        'alpha_is_foo',
        'Date Created',
    )


@pytest.fixture
def records(db):
    records = (
        Model(alpha='foo'),
        Model(alpha='bar'),
        Model(alpha='foo'),
    )
    db.add_all(records)
    db.commit()
    return records


class TestCSVFormatter:
    @pytest.fixture
    def expected(self):
        today = datetime.datetime.now().strftime('%Y-%m-%d')
        return [
            'alpha_is_foo,Date Created',
            f'True,{today}',
            f'False,{today}',
            f'True,{today}',
        ]

    def test_to_string(self, records, expected):
        report = MockReport()
        output = CSVFormatter(report).to_string()

        assert output.splitlines() == expected

    def test_to_file(self, records, expected, tmpdir):
        output_file = tmpdir.mkdir('test').join('output.csv')
        with open(output_file, 'w') as fp:
            CSVFormatter(MockReport()).to_file(fp)
        with open(output_file, 'r') as fp:
            output = [
                line.strip()  # Strip newlines
                for line
                in fp.readlines()
            ]
        assert output == expected


class TestJSONFormatter:
    @pytest.fixture
    def expected(self):
        today = datetime.datetime.now().strftime('%Y-%m-%d')
        return [
            {
                'alpha_is_foo': True,
                'Date Created': today,
            },
            {
                'alpha_is_foo': False,
                'Date Created': today,
            },
            {
                'alpha_is_foo': True,
                'Date Created': today,
            },
        ]

    def test_to_string(self, records, expected):
        report = MockReport()

        output = JSONFormatter(report).to_string()
        assert json.loads(output) == expected

    def test_to_file(self, records, expected, tmpdir):
        output_file = tmpdir.mkdir('test').join('output.json')
        with open(output_file, 'w') as fp:
            JSONFormatter(MockReport()).to_file(fp)
        with open(output_file, 'r') as fp:
            output = json.load(fp)
        assert output == expected
