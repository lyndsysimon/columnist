from setuptools import find_packages
from setuptools import setup

import columnist


with open('README.rst', 'r') as f:
    long_description = f.read()


setup(
    name=columnist.name,
    version=columnist.version,
    author='Lyndsy Simon',
    author_email='lyndsy@lyndsysimon.com',
    description='Simplify creating reports on SQLAlchemy models',
    long_description=long_description,
    url='https://github.com/pypa/columnist',
    packages=find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
