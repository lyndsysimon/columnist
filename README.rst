Columnist
=========

==========     =================
``master``     |gitlab_ci_badge|
==========     =================

Quickstart
----------

At its most basic, ``columnist`` is designed to make it easy to generate a
structured reports based on a model:

.. code-block:: python

    from columnist import Report, column

    from my_app.models import User

    class RegisteredUsersReport(Report):
        query = User.query.filter(active=True).all()

        @column(label='User Name')
        def name(self, row):
            return f'{row.given_name} {row.family_name}'

        @column
        def email(self, row):
            return row.email

Project Roadmap
---------------

``columnist`` is currently incomplete, but under active development. Upcoming
features include:

- [ ] Explicit column ordering in output, other than the order in which the
  are defined
- [ ] Simpler definition of columns that are simply untransformed row attributes
- [ ] Aggregation based on a time series

  - [ ] Implicit strategy
  - [ ] Explicit strategy

- [ ] Report generation in multiple formats

  - [x] CSV
  - [ ] JSON
  - [ ] XML
  - [ ] Plugin architecture for arbitrary output

- [ ] Explicit support and tests for ORMs other than SQLAlchemy

  - [ ] Django ORM
  - [ ] raw SQL
  - [ ] arbitrary iterables

.. |gitlab_ci_badge| image:: https://gitlab.com/lyndsysimon/columnist/badges/master/pipeline.svg
